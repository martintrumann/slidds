use bevy::prelude::Resource;
use std::sync::{Condvar, Mutex};

#[derive(Resource)]
pub struct Udp(std::net::UdpSocket, std::sync::Arc<(Mutex<bool>, Condvar)>);

impl Udp {
    #[cfg(not(unix))]
    pub fn new() -> Selg {
        unimplemented!()
    }

    #[cfg(unix)]
    pub fn new() -> Self {
        let s = std::net::UdpSocket::bind("0.0.0.0:5722").unwrap();
        s.set_nonblocking(true).unwrap();
        let fd = std::os::fd::AsRawFd::as_raw_fd(&s);
        let b = std::sync::Arc::new((Mutex::new(false), Condvar::new()));
        let bb = b.clone();
        std::thread::spawn(move || loop {
            let (lock, cv) = &*bb;
            let l = lock.lock().unwrap();
            #[allow(clippy::bool_comparison)]
            let mut l = cv.wait_while(l, |should_poll| *should_poll == false)
                .unwrap();
            nix::poll::poll(
                &mut [nix::poll::PollFd::new(
                    &unsafe { std::os::fd::BorrowedFd::borrow_raw(fd) },
                    nix::poll::PollFlags::POLLIN,
                )],
                -1,
            )
            .unwrap();
            *l = false;
        });
        Self(s, b)
    }

    fn polling(&self) -> bool {
        match self.1 .0.try_lock() {
            Ok(b) => *b,
            Err(std::sync::TryLockError::WouldBlock) => true,
            Err(std::sync::TryLockError::Poisoned(_)) => panic!(),
        }
    }

    fn poll(&self) {
        *self.1 .0.lock().unwrap() = true;
        self.1 .1.notify_one();
    }

    #[cfg(not(unix))]
    pub fn read(&self) -> Option<String> {
        unimplimented!();
    }

    #[cfg(unix)]
    pub fn read(&self) -> Option<String> {
        if self.polling() {
            return None;
        }
        let mut buf = [0; 8];
        match self.0.recv_from(&mut buf) {
            Ok((0, _)) => None,
            Ok((c, p)) => {
                let s = std::str::from_utf8(&buf[..c]).unwrap().to_owned();
                println!("udp from {}: {s}", p.ip());
                Some(s)
            }
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                self.poll();
                None
            }
            Err(e) => {
                eprintln!("udp error: {e}");
                None
            }
        }
    }
}
