use std::{num::ParseFloatError, path::PathBuf};

use bevy::{
    asset::{io::Reader, AssetLoader, AsyncReadExt, LoadContext},
    prelude::*,
    reflect::{TypePath, TypeUuid},
    utils::BoxedFuture,
};

#[derive(Debug)]
pub enum ParseError {
    BadLoc,
    NotFound,
    BadImg,
    BadVar,
}

impl From<ParseFloatError> for ParseError {
    fn from(_value: ParseFloatError) -> Self {
        Self::BadLoc
    }
}

impl std::error::Error for ParseError {}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed to parse slide: {:?}", self)
    }
}

#[derive(Debug, TypeUuid, TypePath, Asset)]
#[uuid = "5bff5a98-7856-4711-bc3c-67d3aab011cc"]
pub struct Slides(pub Config, pub Vec<Slide>);

#[derive(Debug)]
pub struct Config {
    pub title_font: Option<String>,
    pub title_size: f32,
    pub gap: f32,
    pub content_font: Option<String>,
    pub content_size: f32,
}

#[derive(Debug)]
pub struct Slide {
    /// \[x, y, rotation\]
    pub loc: [f32; 3],
    pub title: Option<(String, [f32; 2])>,
    pub content: Vec<(String, Option<[f32; 2]>)>,
    pub images: Vec<([f32; 3], f32, PathBuf)>,
}

impl Slide {
    pub fn spawn(&self, c: &mut Commands, a: &AssetServer, conf: &Config) -> Entity {
        c.spawn(SpatialBundle::from_transform(Transform {
            translation: Vec3::new(self.loc[0], self.loc[1], 0.),
            rotation: Quat::from_rotation_z(self.loc[2].to_radians()),
            ..Default::default()
        }))
        .with_children(|p| {
            let Some(ref title) = self.title else {
                return;
            };
            let anchor = bevy::sprite::Anchor::TopLeft;
            let color = Color::BLACK;
            let mut loc = Vec2::from(title.1);
            p.spawn(Text2dBundle {
                text: Text::from_section(
                    title.0.clone(),
                    TextStyle {
                        font_size: conf.title_size,
                        color,
                        ..Default::default()
                    },
                ),
                text_anchor: anchor.clone(),
                transform: Transform::from_translation(loc.extend(0.)),
                ..Default::default()
            });

            let style = TextStyle {
                font_size: conf.content_size,
                color,
                ..Default::default()
            };
            loc.y -= conf.gap;
            for (c, l) in self.content.iter() {
                if let Some(l) = l {
                    loc = Vec2::from(*l);
                }
                p.spawn(Text2dBundle {
                    text: Text::from_section(c, style.clone()),
                    transform: Transform::from_translation(loc.extend(0.)),
                    text_anchor: anchor.clone(),
                    ..Default::default()
                });
                loc.y -= conf.content_size;
            }

            for i in self.images.iter() {
                p.spawn(SpriteBundle {
                    transform: Transform::from_translation(Vec3::new(i.0[0], i.0[1], 0.))
                        .with_scale((Vec2::ONE * i.1).extend(1.)),
                    texture: a.load(i.2.clone()),
                    ..Default::default()
                });
            }
        })
        .id()
    }
}

fn parse_loc(s: &str) -> Result<[f32; 3], ParseError> {
    let mut spl = s[1..(s.len() - 1)].split(',');
    let x = spl.next().ok_or(ParseError::BadLoc)?.trim().parse()?;
    let y = spl.next().ok_or(ParseError::BadLoc)?.trim().parse()?;
    let rot = spl.next().unwrap_or("0").trim().parse()?;
    Ok([x, y, rot])
}

impl Slides {
    fn from(s: String) -> Result<Self, ParseError> {
        let (top, s) = s.split_once("\n+++\n").unwrap_or(("", s.as_str()));

        let mut c = Config {
            title_font: None,
            title_size: 50.,
            gap: 60.,
            content_font: None,
            content_size: 30.,
        };
        for v in top.lines() {
            let (var, val) = v.split_once(" = ").ok_or(ParseError::BadVar)?;
            match var {
                "font" => {
                    c.title_font = Some(val.into());
                    c.content_font = Some(val.into());
                }
                "title" => {
                    if val.chars().all(|c| c.is_ascii_digit() || c == '.') {
                        c.title_size = val.parse::<f32>().unwrap();
                    } else {
                        todo!("both font and size defining")
                    }
                }
                "content" => {
                    if val.chars().all(|c| c.is_ascii_digit() || c == '.') {
                        c.content_size = val.parse::<f32>().unwrap();
                    } else {
                        todo!("both font and size defining")
                    }
                }
                "gap" => c.gap = val.parse().map_err(|_| ParseError::BadVar)?,
                _ => return Err(ParseError::BadVar),
            }
        }

        let mut v = Vec::new();
        for s in s.split("\n---\n") {
            let mut lines = s.lines();
            let loc = lines.next().unwrap();
            if loc.starts_with('#') {
                continue;
            } else if !loc.starts_with('[') {
                return Err(ParseError::BadLoc);
            }
            let title = lines
                .next()
                .map(|s| {
                    if s.contains('[') {
                        let (s, loc) = s.split_once('[').ok_or(ParseError::BadLoc)?;
                        let (x, y) = loc
                            .trim_matches(']')
                            .split_once(',')
                            .ok_or(ParseError::BadLoc)?;
                        let loc = [x.trim().parse()?, y.trim().parse()?];
                        Ok::<_, ParseError>((s.to_owned(), loc))
                    } else {
                        Ok((s.trim().into(), [0., 0.]))
                    }
                })
                .transpose()?;

            let mut content = Vec::new();
            for l in lines
                .by_ref()
                .take_while(|s| !s.starts_with("img"))
                .filter(|s| !s.is_empty())
            {
                if l.contains(" [") {
                    let (s, loc) = l.split_once(" [").unwrap();
                    let (x, y) = loc
                        .trim_matches(']')
                        .split_once(',')
                        .ok_or(ParseError::BadLoc)?;
                    content.push((
                        String::from(s.trim()),
                        Some([x.trim().parse()?, y.trim().parse()?]),
                    ))
                } else {
                    content.push((String::from(l.replace(r"\[", "[")), None))
                }
            }

            let images = lines
                .filter(|s| !(s.is_empty() || s.starts_with('#')))
                .map(|s| {
                    let mut iter = s.split(" ");
                    let path = iter.next().ok_or(ParseError::BadImg)?;
                    let loc = parse_loc(iter.next().ok_or(ParseError::BadLoc)?)?;
                    let scale = iter
                        .next()
                        .ok_or(ParseError::BadImg)?
                        .trim_matches('x')
                        .parse()?;
                    let path = PathBuf::from(path);
                    Ok::<_, ParseError>((loc, scale, path))
                })
                .collect::<Result<Vec<(_, _, _)>, _>>()?;

            v.push(Slide {
                loc: parse_loc(loc)?,
                title,
                content,
                images,
            })
        }

        Ok(Self(c, v))
    }
}

#[derive(Default)]
pub struct SlideLoader;

impl AssetLoader for SlideLoader {
    type Asset = Slides;
    type Settings = ();
    type Error = ParseError;

    fn load<'a>(
        &'a self,
        reader: &'a mut Reader,
        _: &'a Self::Settings,
        _: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<Self::Asset, ParseError>> {
        Box::pin(async move {
            let mut buf = String::new();
            reader
                .read_to_string(&mut buf)
                .await
                .map_err(|_| ParseError::NotFound)?;
            Slides::from(buf)
        })
    }

    fn extensions(&self) -> &[&str] {
        &["sld"]
    }
}
