#[cfg(not(debug_assertions))]
use bevy::winit::{UpdateMode, WinitSettings};

use bevy::{prelude::*, window::RequestRedraw};

mod slide;
mod udp;

fn main() {
    let app = &mut App::new();
    app.add_plugins(
        DefaultPlugins
            .set(WindowPlugin {
                primary_window: Some(Window {
                    #[cfg(debug_assertions)]
                    title: "slidds --DEV--".into(),
                    #[cfg(not(debug_assertions))]
                    title: "Slidds".into(),
                    ..Default::default()
                }),
                ..Default::default()
            })
            .set(AssetPlugin {
                #[cfg(not(debug_assertions))]
                asset_folder: String::from("."),
                ..Default::default()
            }),
    )
    .init_asset::<slide::Slides>()
    .init_asset_loader::<slide::SlideLoader>()
    .insert_resource(ClearColor(Color::WHITE))
    .insert_resource(Slides(Vec::new()))
    .insert_resource(CurSlide(0));

    if std::env::var("slidds_udp").is_ok() {
        info!("using udp");
        app.insert_resource(udp::Udp::new())
            .add_systems(PreUpdate, mv_cam_fs);
    }

    app.add_systems(Startup, setup)
        .add_systems(
            PreUpdate,
            (
                set_cam_target,
                move_cam_target.run_if(resource_changed::<CurSlide>()),
                slide_cam,
                #[cfg(not(debug_assertions))]
                set_to_desktop_app.run_if(some_slides()),
            )
                .chain(),
        )
        .add_systems(Update, (slide_changed, bevy::window::close_on_esc))
        .run()
}

#[cfg(not(debug_assertions))]
fn some_slides() -> impl FnMut(Res<Slides>) -> bool + Clone {
    let mut has_run = false;
    move |r: Res<Slides>| {
        if !has_run && !r.0.is_empty() {
            has_run = true;
            true
        } else {
            false
        }
    }
}

#[cfg(not(debug_assertions))]
fn set_to_desktop_app(mut r: ResMut<WinitSettings>, p: Option<Res<udp::Udp>>) {
    *r = WinitSettings::desktop_app();
    if p.is_some() {
        r.focused_mode = UpdateMode::Reactive {
            max_wait: Duration::from_millis(100),
        }
    }
}

fn mv_cam_fs(so: Res<udp::Udp>, mut r: ResMut<CurSlide>) {
    let Some(s) = so.read() else { return };

    if ["+", "n"].contains(&s.as_str()) {
        r.mv(1)
    } else if ["-", "p"].contains(&s.as_str()) {
        r.mv(-1)
    } else if let Ok(n) = s.parse::<u8>() {
        r.set(n as usize)
    } else {
        eprintln!("command not found: {s:?}")
    }
}

#[derive(Component)]
struct Target(Vec2);

fn slide_cam(
    mut q: Query<(&mut Transform, &Target), With<Camera2d>>,
    mut total: Local<Option<f32>>,
    mut redraw: EventWriter<RequestRedraw>,
) {
    let (mut t, tr) = q.single_mut();
    if t.translation.truncate() == tr.0 {
        return;
    }
    redraw.send(RequestRedraw);
    let z = t.translation.z;
    let delta = tr.0.extend(z) - t.translation;
    debug_assert_eq!(delta.z, 0.);
    let len = delta.length();
    let x = if let Some(t) = *total {
        len / t
    } else {
        *total = Some(len);
        1.
    };
    // TODO: make all movements take the same ammount of time
    let speed = (1. - f32::powf(2., -10. * x)) * 140.;
    if speed < 0.5 || len <= speed {
        t.translation = tr.0.extend(z);
        *total = None;
    } else {
        t.translation += delta.normalize() * speed;
    }
}

#[derive(Resource)]
struct CurSlide(usize);
impl CurSlide {
    fn set(&mut self, n: usize) {
        self.0 = n;
    }

    fn mv(&mut self, u: isize) {
        match u.signum() {
            1 => self.0 += u as usize,
            -1 if self.0 > 0 => self.0 -= u.unsigned_abs(),
            _ => (),
        }
    }
}

fn set_cam_target(inp: Res<Input<KeyCode>>, mut slide: ResMut<CurSlide>) {
    if inp.any_just_pressed([KeyCode::Right, KeyCode::Space]) {
        slide.mv(1);
    } else if inp.any_just_pressed([KeyCode::Left]) {
        slide.mv(-1);
    } else if inp.just_pressed(KeyCode::Key1) {
        slide.set(0);
    } else if inp.just_pressed(KeyCode::Key2) {
        slide.set(1);
    } else if inp.just_pressed(KeyCode::Key3) {
        slide.set(2);
    } else if inp.just_pressed(KeyCode::Key4) {
        slide.set(3);
    } else if inp.just_pressed(KeyCode::Key5) {
        slide.set(4);
    } else if inp.just_pressed(KeyCode::Key6) {
        slide.set(5);
    } else if inp.just_pressed(KeyCode::Key7) {
        slide.set(6);
    } else if inp.just_pressed(KeyCode::Key8) {
        slide.set(7);
    } else if inp.just_pressed(KeyCode::Key9) {
        slide.set(8);
    } else if inp.just_pressed(KeyCode::Key0) {
        slide.set(9);
    }
}

fn move_cam_target(
    cur: Res<CurSlide>,
    slides: Res<Slides>,
    mut q: Query<&mut Target, With<Camera2d>>,
    // win: Query<&Window>,
) {
    // let w = win.single();
    // let s = Vec2::new(w.width(), -w.height()) / 2.;
    let Some(t) = slides.0.get(cur.0) else { return };
    q.single_mut().0 = t.0;
}

#[derive(Resource)]
struct Slides(Vec<(Vec2, f32)>);

#[derive(Component)]
struct AllSlides;

fn setup(mut c: Commands, a: Res<AssetServer>) {
    c.spawn((Camera2dBundle::default(), Target(Vec2::ZERO)));

    let sld: Handle<slide::Slides> = a.load("main.sld");

    c.spawn((SpatialBundle::default(), AllSlides, sld));
}

fn slide_changed(
    mut c: Commands,
    q: Query<Entity, With<AllSlides>>,
    mut ev: EventReader<AssetEvent<slide::Slides>>,
    a: Res<AssetServer>,
    r: Res<Assets<slide::Slides>>,
    mut slides: ResMut<Slides>,
) {
    let all_slides = q.single();
    for e in ev.read() {
        match e {
            AssetEvent::Added { id } | AssetEvent::Modified { id } => {
                if let Some(s) = r.get(*id) {
                    let mut v_id = Vec::new();
                    let mut v = Vec::new();
                    let conf = &s.0;
                    for s in s.1.iter() {
                        v_id.push(s.spawn(&mut c, &a, conf));
                        v.push((Vec2::new(s.loc[0], s.loc[1]), s.loc[2]));
                    }
                    slides.0 = v;
                    c.entity(all_slides)
                        .despawn_descendants()
                        .push_children(&v_id);
                }
            }
            AssetEvent::LoadedWithDependencies { .. } => (),
            AssetEvent::Removed { .. } => unreachable!(),
        }
    }
}
