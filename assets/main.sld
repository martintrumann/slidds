title = 50
gap = 70
content = 30
+++
[0, 0, 0]
# Rust type system [-400, 100]
Martin Trumann
8 November 2023

img
./rust-logo.png [250,0] x0.5

---
[1400, -400]
# Refrences [-500, 100]
Jung, Jacques-Henri Jourdan, Robbert Krebbers, and Derek Dreyer. 2017.
RustBelt: securing the foundations of the Rust programming language.
Poc. ACM Program. Lang. 2, POPL, Article 66 (January 2018), 34 pages.
https://doi.org/10.1145/3158154 (retrived: 02.11.2023)
Section 2: A tour of Rust

---
[0, -620]
# Rust Type System [-220, 100]
1. Ownership
2. Mutable Refrences
3. Shared Refrences
4. Lifetimes
5. Interior Mutability

---
[1200, -1050]
# Rust types: Ownership [-350,210]
- Basis of entire type system.
- Movable into other functions or threads.
- No garbage collection. Destructors used instead.

img
./ownership.png [0,-100] x0.4
---
[1250, -1600]
# Rust types: Mutable Refrences [-350,60]

- Temporary exclusive access.
- Keep using variable after destruction.
- No other access to refrence.
- No copies of the refrence.

---
[0, -1600]
# Rust types: Shared Refrences [-350, 60]

- Temporary read-only access.
- Allow duplication. Prohibit mutation.
- Keep using variable after destruction.

---
[-1200, -1500]
# Rust types: Lifetimes [-350, 210]

- Describe how long refrence lives.
- Ensures the rules are upheld.
- Mostly invisible.

img
./lifetimes.png [0,-80] x0.5
---
[-1250, -900]
# Rust types: Interior Mutability [-400,60]

- Couple of types in the standard library.
- Carefully created API.
- Safe because of compile or runtime checks.

---
[0, -620]
---
# vim:ft=text
