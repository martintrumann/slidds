.PHONY: default, build, run, run_delay, test

features := --features=bevy/dynamic_linking,bevy/file_watcher

default: build

build:
	cargo build ${features}

run:
	cargo run ${features}

run_delay:
	sleep 1
	cargo run ${features}

test:
	cargo test ${features}
